Laptop configs
=========

Configuration files for my laptop.

Requirements
------------

Arch Linux

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Use locally:

    - hosts: local
      roles:
         - { role: ansible-laptop }

License
-------

Unlisence

Author Information
------------------

<https://axilleas.me/about/>
